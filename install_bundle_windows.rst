.. _bundle_windows:

Installing on Windows
---------------------

On Windows, the easiest way to install HyperSpy is to use the HyperSpy bundle
installer. This simple to install program provides a customized Anaconda 
installation, which contains the HyperSpy libraries but also other libraries 
used in the field of electron microscopy. A detailed walk through of the 
process is provided below.

Download
^^^^^^^^

First, download the bundle installer using the following link:

- https://github.com/hyperspy/hyperspy-bundle/releases/download/2021.06.11/HyperSpy-bundle-2021.06.11-Windows-x86_64-AMD.exe


Installing
^^^^^^^^^^

Run the downloaded file to proceed with the installation. This process is fairly
straightforward. For the installation location, we *highly* recommend to install
as single user in a folder that does not require administrative rights, as set 
by default.

.. figure:: _static/windows_single_user.jpg
   :width: 100 %
   :alt: Windows single user installation
   :figwidth: 50%

   Single user installation is recommended.

It is recommended to install the HyperSpy bundle in a folder with a short path
(less than 32 characters) to avoid issues with the JupyterLab and its deeply
nested folder structure. The default folder should be fine in most situations
(non domain user account):

.. figure:: _static/windows_install_location.jpg
   :width: 100 %
   :alt: Windows installation location
   :figwidth: 50%

   It is recommended to install the HyperSpy bundle in a folder with a short
   path (less than 32 characters).

Keep the default options unless you know what you are doing:

.. figure:: _static/windows_default_options.jpg
   :width: 100 %
   :alt: Bundle install options.
   :figwidth: 50%

   A screenshot showing the default options. If a different Python distribution is
   installed on the system, by default this distribution will not be registered
   as the default Python distribution.

Doing so will install HyperSpy into your user folder under a subfolder named
``"HyperSpy-bundle"``. The installation may take some time.

After the installer is finished, right click your desktop, and see that "Jupyter lab here", "Jupyter notebook here" and "Jupyter qtconsole here" has been added:

.. figure:: _static/windows_install_context_menu.jpg
   :width: 100 %
   :alt: Right click context menu
   :figwidth: 50%

   Image showing how the "right click" menu should look like after the installation.
      

And that's it!

If you had any problems during the installation, you can get support on the workshop's gitter channel: https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021
