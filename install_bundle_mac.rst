.. _bundle_macos:

Installing on macOS
-------------------

On macOS, the easiest way to install HyperSpy is to use the HyperSpy bundle
installer. This simple to install program provides a customized Anaconda 
installation, which contains the HyperSpy libraries but also other libraries 
used in the field of electron microscopy. A detailed walk through of the 
process is provided below.

Download
^^^^^^^^

First, download the bundle installer using the following link:

- https://github.com/hyperspy/hyperspy-bundle/releases/download/2021.06.11/HyperSpy-bundle-2021.06.11-MacOSX-x86_64-Intel.pkg


Installing
^^^^^^^^^^

Run the downloaded file to proceed with the installation. This installer is
currently not identified as trusted party by macOS, meaning that macOS will
not allow to run the installer simply by double-clicking on it. However, 
control-clicking the app icon, then choosing ``Open`` from the shortcut menu 
will allow to run the installer, as explained in the 
`macOS documentation <https://support.apple.com/en-gb/guide/mac-help/mh40616/mac>`_.

.. figure:: _static/macOS_right_click_open.png
   :width: 60 %
   :alt: Links HyperSpy bundle downloads
   :figwidth: 70%

   To open the installer, control-click the installer icon and choose Open from
   the shortcut menu.

This rest of the process is fairly straightforward. For the installation 
location, we *highly* recommend to select ``Install for me only``:

.. figure:: _static/macOS_install_destination.png
   :width: 100 %
   :alt: Bundle installation progress
   :figwidth: 50%

   Single user installation is recommended.


And that's it!

If you had any problems during the installation, you can get support on the workshop's gitter channel: https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021
