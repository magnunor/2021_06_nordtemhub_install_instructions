.. _bundle_linux:

Installing on Linux
-------------------

On Linux, the easiest way to install HyperSpy is to use the HyperSpy bundle
installer. This simple to install program provides a customized Anaconda 
installation, which contains the HyperSpy libraries but also other libraries 
used in the field of electron microscopy.

Download
^^^^^^^^

First, download the bundle installer using the following link:

- https://github.com/hyperspy/hyperspy-bundle/releases/download/2021.06.11/HyperSpy-bundle-2021.06.11-Linux-x86_64-AMD.sh


Installing
^^^^^^^^^^

The process is exactly the same as 
`installing miniconda <https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html>`_
or anaconda:

1. In your terminal window, run:

   ..  code-block:: bash
    
       bash HyperSpy-bundle-2021.06.11-Linux-x86_64-AMD.sh

2. Follow the prompts on the installer screens.
   If you are unsure about any setting, accept the defaults. You can change
   them later.
3. To make the changes take effect, close and then re-open your terminal window.
4. Test your installation. In your terminal window, run the command 
   ``conda list``. A list of installed packages appears if it has been
   installed correctly.


And that's it!

If you had any problems during the installation, you can get support on the workshop's gitter channel: https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021
