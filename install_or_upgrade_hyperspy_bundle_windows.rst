.. _install_or_upgrade_hyperspy_windows:

Installing and upgrading in Conda environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Instructions if you have an Anaconda, Miniconda or older HyperSpy bundle install,
and either needs to install the packages, or update them.

The command is the same for all the installations, but the prompt can be in one of these locations:

* Start menu - Anaconda3 (64-bit) - Anaconda prompt (anaconda3)
* Start menu - Anaconda3 (64-bit) - Anaconda prompt (miniconda3)
* Start menu - Miniforge3 (64-bit) - Miniforge prompt (HyperSpy-bundle)

In this terminal, write:

    ..  code-block:: bash

        conda update --all


Then make sure the packages we need are installed:

    ..  code-block:: bash

        conda install hyperspy atomap pyxem jupyterlab -c conda-forge


Then, in the same terminal:

    ..  code-block:: bash

        conda update hyperspy atomap pyxem -c conda-forge


If you get some kind of error, you can get support on the workshop's gitter channel: https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021
