.. _install_or_upgrade_hyperspy_macos:

Installing and upgrading in Conda environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Instructions if you have an Anaconda, Miniconda or older HyperSpy bundle install,
and either needs to install the packages, or update them.

Open the terminal, for example by pressing Command + Space, then type ``terminal``.

In this terminal, write:

    ..  code-block:: bash

        conda update --all


Then make sure the packages we need are installed:

    ..  code-block:: bash

        conda install hyperspy atomap pyxem jupyterlab -c conda-forge


Then, in the same terminal:

    ..  code-block:: bash

        conda update hyperspy atomap pyxem -c conda-forge


If you get some kind of error, you can get support on the workshop's gitter channel: https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021
