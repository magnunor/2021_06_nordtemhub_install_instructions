.. _install_advanced:

Advanced installations
----------------------

Anaconda or miniconda
^^^^^^^^^^^^^^^^^^^^^

If you already have an Anaconda or miniconda environment,
you can install all the packages via the Anaconda prompt
(Start menu - Anaconda3 (64-bit) - Anaconda prompt (anaconda3)).
This will open a command line. Here, write:

    ..  code-block:: bash
    
        conda install hyperspy atomap pyxem -c conda-forge


Via PyPI
^^^^^^^^

If you're on linux, you can install via ``pip``. If you do this, it is recommended to install it in a virtual environment.

    ..  code-block:: bash

        pip install hyperspy[all] pyxem atomap


Questions and support
^^^^^^^^^^^^^^^^^^^^^

If you run into any issues, the best place to get help is on the workshop's gitter channel: https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021
