.. _install_anaconda_miniconda:

Installing in existing Anaconda or miniconda
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you already have an Anaconda or miniconda environment,
you can install all the packages via the Anaconda prompt
(Start menu - Anaconda3 (64-bit) - Anaconda prompt (anaconda3)).
This will open a command line. Here, write:

    ..  code-block:: bash
    
        conda install hyperspy atomap pyxem -c conda-forge


This can take a long time.

At the end of the process, the last line should say "done". If you do not see this, you can get support on the workshop's gitter channel: https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021
