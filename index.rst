:tocdepth: 4

.. toctree::
   :hidden:

   install_bundle_windows
   install_bundle_mac
   install_bundle_linux

2021 NordTEMHub workshop: installation instructions
---------------------------------------------------

Select the instruction applicable to your system.

If you're not sure, select **a) You do not have a python distribution**.

a) You do not have a python distribution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Go to the documentation for your operating system:

* :ref:`Windows <bundle_windows>`
* :ref:`macOS <bundle_macos>`
* :ref:`Linux <bundle_linux>`


b) You have a miniconda or anaconda distribution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This also includes a previously installed HyperSpy bundle, which needs to be updated.

Go to the documentation for your operating system:

* :ref:`Windows <install_or_upgrade_hyperspy_windows>`
* :ref:`macOS <install_or_upgrade_hyperspy_macos>`
* :ref:`linux <install_or_upgrade_hyperspy_linux>`


..  caution::
    It is important that you install HyperSpy using either the "Bundle" installer,
    or in an existing Python distribution (*not both!*). Installing
    both can work, but will leave you with multiple Python installations on your
    system, and it will be very confusing to try to solve any issues that arise
    if you are not experienced with Python.

Getting Help
------------

If you have any issues with the installation, or have questions during the workshop,
use:

- `Gitter channel for the workshop <https://gitter.im/TEM-Gemini-Centre/NordTEMHubDigitalWorkshop2021>`_. Note that this requires that you have, or make, a GitHub, Gitlab or a Twitter account.
- `The HyperSpy user guide <http://hyperspy.org/hyperspy-doc/current/user_guide/install.html>`_
